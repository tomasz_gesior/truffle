pragma solidity ^0.4.18;

contract Greetings {
  string message;

  constructor()  {
    message = 'Hello world!';
  }

  function setGreetings(string _message) public {
    message = _message;
  }

  function getGreetings() public view returns(string) {
    return message;
  }
}
